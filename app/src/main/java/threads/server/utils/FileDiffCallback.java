package threads.server.utils;

import androidx.recyclerview.widget.DiffUtil;

import java.util.List;
import java.util.Objects;

import threads.server.core.files.FileInfo;

@SuppressWarnings("WeakerAccess")
public class FileDiffCallback extends DiffUtil.Callback {
    private final List<FileInfo> mOldList;
    private final List<FileInfo> mNewList;

    public FileDiffCallback(List<FileInfo> messages, List<FileInfo> fileInfos) {
        this.mOldList = messages;
        this.mNewList = fileInfos;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return areItemsTheSame(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return areContentsTheSame(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }

    public boolean areItemsTheSame(FileInfo oldInfo, FileInfo newInfo) {
        return oldInfo.getIdx() == newInfo.getIdx();
    }


    public boolean areContentsTheSame(FileInfo oldInfo, FileInfo newInfo) {
        return oldInfo.isDeleting() == newInfo.isDeleting() &&
                oldInfo.getLastModified() == newInfo.getLastModified() &&
                Objects.equals(oldInfo.getSize(), newInfo.getSize()) &&
                Objects.equals(oldInfo.getWork(), newInfo.getWork()) &&
                Objects.equals(oldInfo.getName(), newInfo.getName()) &&
                Objects.equals(oldInfo.getMimeType(), newInfo.getMimeType()) &&
                Objects.equals(oldInfo.getUri(), newInfo.getUri());
    }
}
