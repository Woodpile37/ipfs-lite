/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.tls;

public class TlsConstants {


    // https://tools.ietf.org/html/rfc8446#appendix-B.4  Cipher Suites
    public static byte[] TLS_AES_128_GCM_SHA256 = new byte[]{0x13, 0x01};

    public static byte[] TLS_AES_256_GCM_SHA384 = new byte[]{0x13, 0x02};

    public static byte[] TLS_CHACHA20_POLY1305_SHA256 = new byte[]{0x13, 0x03};
    public static byte[] TLS_AES_128_CCM_SHA256 = new byte[]{0x13, 0x04};

    public static byte[] TLS_AES_128_CCM_8_SHA256 = new byte[]{0x13, 0x05};


    public enum ContentType {
        invalid(0),
        change_cipher_spec(20),
        alert(21),
        handshake(22),
        application_data(23),
        ;

        public final byte value;

        ContentType(int value) {
            this.value = (byte) value;
        }
    }


    public enum ExtensionType {
        server_name(0),                             /* RFC 6066 */
        max_fragment_length(1),                     /* RFC 6066 */
        status_request(5),                          /* RFC 6066 */
        supported_groups(10),                       /* RFC 8422, 7919 */
        signature_algorithms(13),                   /* RFC 8446 */
        use_srtp(14),                               /* RFC 5764 */
        heartbeat(15),                              /* RFC 6520 */
        application_layer_protocol_negotiation(16), /* RFC 7301 */
        signed_certificate_timestamp(18),           /* RFC 6962 */
        client_certificate_type(19),                /* RFC 7250 */
        server_certificate_type(20),                /* RFC 7250 */
        padding(21),                                /* RFC 7685 */
        pre_shared_key(41),                         /* RFC 8446 */
        early_data(42),                             /* RFC 8446 */
        supported_versions(43),                     /* RFC 8446 */
        cookie(44),                                 /* RFC 8446 */
        psk_key_exchange_modes(45),                 /* RFC 8446 */
        certificate_authorities(47),                /* RFC 8446 */
        oid_filters(48),                            /* RFC 8446 */
        post_handshake_auth(49),                    /* RFC 8446 */
        signature_algorithms_cert(50),              /* RFC 8446 */
        key_share(51),
        ;

        public final short value;

        ExtensionType(int value) {
            this.value = (short) value;
        }
    }


    public enum PskKeyExchangeMode {
        psk_ke(0),
        psk_dhe_ke(1);

        public final byte value;

        PskKeyExchangeMode(int value) {
            this.value = (byte) value;
        }
    }

    public enum CertificateType {
        X509(0),
        RawPublicKey(2),
        ;

        public final byte value;

        CertificateType(int value) {
            this.value = (byte) value;
        }
    }


    public enum AlertDescription {
        close_notify(0),
        unexpected_message(10),
        bad_record_mac(20),
        record_overflow(22),
        handshake_failure(40),
        bad_certificate(42),
        unsupported_certificate(43),
        certificate_revoked(44),
        certificate_expired(45),
        certificate_unknown(46),
        illegal_parameter(47),
        unknown_ca(48),
        access_denied(49),
        decode_error(50),
        decrypt_error(51),
        protocol_version(70),
        insufficient_security(71),
        internal_error(80),
        inappropriate_fallback(86),
        user_canceled(90),
        missing_extension(109),
        unsupported_extension(110),
        unrecognized_name(112),
        bad_certificate_status_response(113),
        unknown_psk_identity(115),
        certificate_required(116),
        no_application_protocol(120);

        public final byte value;

        AlertDescription(int value) {
            this.value = (byte) value;
        }
    }
}
