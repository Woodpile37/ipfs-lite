package threads.lite.cid;

public class Dir {
    private final Cid cid;
    private final long size;

    public Dir(Cid cid, long size) {
        this.cid = cid;
        this.size = size;
    }

    public Cid getCid() {
        return cid;
    }

    public long getSize() {
        return size;
    }
}
