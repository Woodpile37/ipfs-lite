package threads.lite.core;

import androidx.annotation.NonNull;

import java.util.List;

import threads.lite.IPFS;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.PeerId;

public class PeerInfo {

    @NonNull
    private final String agent;
    @NonNull
    private final String version;
    @NonNull
    private final Multiaddrs multiaddrs;
    @NonNull
    private final List<String> protocols;
    @NonNull
    private final Multiaddr observed;
    @NonNull
    private final PeerId peerId;

    public PeerInfo(@NonNull PeerId peerId, @NonNull String agent, @NonNull String version,
                    @NonNull Multiaddrs multiaddrs, @NonNull List<String> protocols,
                    @NonNull Multiaddr observed) {
        this.agent = agent;
        this.version = version;
        this.multiaddrs = multiaddrs;
        this.protocols = protocols;
        this.peerId = peerId;
        this.observed = observed;
    }

    @NonNull
    public PeerId getPeerId() {
        return peerId;
    }

    @NonNull
    public String getVersion() {
        return version;
    }

    @NonNull
    public List<String> getProtocols() {
        return protocols;
    }

    @NonNull
    public Multiaddr getObserved() {
        return observed;
    }

    @NonNull
    public Multiaddrs getMultiaddrs() {
        return multiaddrs;
    }

    @NonNull
    @Override
    public String toString() {
        return "PeerInfo{" +
                " peerId='" + peerId + '\'' +
                ", agent='" + agent + '\'' +
                ", version='" + version + '\'' +
                ", addresses=" + multiaddrs +
                ", protocols=" + protocols +
                ", observed=" + observed +
                '}';
    }

    @NonNull
    public String getAgent() {
        return agent;
    }

    public boolean hasProtocol(String protocol) {
        return protocols.contains(protocol);
    }

    public boolean hasRelayHop() {
        return hasProtocol(IPFS.RELAY_PROTOCOL_HOP);
    }

}
