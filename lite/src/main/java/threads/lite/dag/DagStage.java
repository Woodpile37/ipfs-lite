package threads.lite.dag;

import androidx.annotation.NonNull;

import merkledag.pb.Merkledag;
import unixfs.pb.Unixfs;

public class DagStage {
    private final Merkledag.PBNode node;
    private Unixfs.Data data;
    private int index;

    DagStage(@NonNull Merkledag.PBNode node) {
        this.node = node;
        this.index = 0;
    }

    public Merkledag.PBNode getNode() {
        return node;
    }

    public Unixfs.Data getData() throws Exception {
        if (data == null) {
            data = DagReader.getData(node);
        }
        return data;
    }

    public void incrementIndex() {
        index = index + 1;
    }

    public int index() {
        return index;
    }

    public void setIndex(int value) {
        index = value;
    }

    @NonNull
    @Override
    public String toString() {
        return node + " " + index + " ";
    }
}
