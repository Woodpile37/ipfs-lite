package threads.lite.dag;

import android.util.Pair;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import merkledag.pb.Merkledag;
import threads.lite.IPFS;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.core.BlockStore;
import threads.lite.utils.ReaderInputStream;
import unixfs.pb.Unixfs;


public final class DagWriter {
    public static final int depthRepeat = 4;
    private final BlockStore blockStore;
    private final ReaderInputStream reader;


    public DagWriter(@NonNull BlockStore blockStore, @NonNull ReaderInputStream reader) {
        this.blockStore = blockStore;
        this.reader = reader;
    }

    public FileWrapper createFileWrapper() {
        return new FileWrapper();
    }

    public void fillNodeLayer(@NonNull FileWrapper node) throws Exception {
        byte[] bytes = new byte[IPFS.CHUNK_SIZE];
        int numChildren = 0;
        while ((numChildren < IPFS.LINKS_PER_BLOCK) && !isDone()) {
            int read = reader.read(bytes);
            if (read > 0) {
                Unixfs.Data.Builder data =
                        Unixfs.Data.newBuilder().setType(Unixfs.Data.DataType.Raw)
                                .setFilesize(read)
                                .setData(ByteString.copyFrom(bytes, 0, read));

                Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
                pbn.setData(ByteString.copyFrom(data.build().toByteArray()));

                Block block = Block.createBlock(pbn.build());
                blockStore.storeBlock(block);
                node.addChild(block.getCid(), read);
            }
            numChildren++;
        }
        node.commit();
    }


    public Cid trickle() throws Exception {
        FileWrapper newRoot = createFileWrapper();
        Pair<Cid, Long> result = fillTrickleRec(newRoot, -1);
        return result.first;
    }


    private Pair<Cid, Long> fillTrickleRec(@NonNull FileWrapper node, int maxDepth) throws Exception {
        // Always do this, even in the base case
        fillNodeLayer(node);


        for (int depth = 1; maxDepth == -1 || depth < maxDepth; depth++) {
            if (isDone()) {
                break;
            }

            for (int repeatIndex = 0; repeatIndex < depthRepeat && !isDone(); repeatIndex++) {

                Pair<Cid, Long> result = fillTrickleRec(createFileWrapper(), depth);

                node.addChild(result.first, result.second);
            }
        }
        Block block = node.commit();
        long fileSize = node.getFileSize();
        blockStore.storeBlock(block);
        return Pair.create(block.getCid(), fileSize);
    }

    public boolean isDone() {
        return reader.done();
    }

    public static class FileWrapper {
        private final Unixfs.Data.Builder data;
        private final Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        private long size = 0L;

        private FileWrapper() {
            this.data = Unixfs.Data.newBuilder()
                    .setType(Unixfs.Data.DataType.File)
                    .setFilesize(size);
        }

        public void addChild(@NonNull Cid cid, long fileSize) {
            Merkledag.PBLink.Builder lnb = Merkledag.PBLink.newBuilder()
                    .setName("")
                    .setTsize(fileSize);
            lnb.setHash(ByteString.copyFrom(cid.encoded()));

            pbn.addLinks(lnb.build());
            data.addBlocksizes(fileSize);
            size = size + fileSize;
        }

        public Block commit() throws Exception {
            data.setFilesize(size);
            pbn.setData(ByteString.copyFrom(data.build().toByteArray()));
            return Block.createBlock(pbn.build());
        }

        public long getFileSize() {
            return size;
        }
    }
}
