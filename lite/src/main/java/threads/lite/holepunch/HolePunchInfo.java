package threads.lite.holepunch;

import androidx.annotation.NonNull;

import java.net.DatagramSocket;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Parameters;
import threads.lite.core.Session;

public class HolePunchInfo {

    @NonNull
    private final Session session;
    @NonNull
    private final Multiaddr observed;
    @NonNull
    private final DatagramSocket socket;
    @NonNull
    private final Parameters parameters;


    public HolePunchInfo(@NonNull Session session, @NonNull Multiaddr observed,
                         @NonNull DatagramSocket socket, @NonNull Parameters parameters) {
        this.session = session;
        this.observed = observed;
        this.socket = socket;
        this.parameters = parameters;
    }

    @NonNull
    public Session getSession() {
        return session;
    }

    @NonNull
    public PeerId getPeerId() {
        return observed.getPeerId();
    }

    @NonNull
    public DatagramSocket getSocket() {
        return socket;
    }

    @NonNull
    public Parameters getParameters() {
        return parameters;
    }

    @NonNull
    public Multiaddr getObserved() {
        return observed;
    }
}
