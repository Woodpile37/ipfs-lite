/*
 * Copyright 2015-2022 the original author or authors
 *
 * This software is licensed under the Apache License, Version 2.0,
 * the GNU Lesser General Public License version 2 or later ("LGPL")
 * and the WTFPL.
 * You may choose either license to govern your use of this software only
 * upon the condition that you accept all of the terms of either
 * the Apache License 2.0, the LGPL 2.1+ or the WTFPL.
 */
package threads.lite.minidns;

import java.io.IOException;

public abstract class DnsException extends IOException {

    protected DnsException(String message) {
        super(message);
    }

    public static class IdMismatch extends DnsException {

        public IdMismatch(DnsMessage request, DnsMessage response) {
            super(getString(request, response));
            assert request.id != response.id;

        }

        private static String getString(DnsMessage request, DnsMessage response) {
            return "The response's ID doesn't matches the request ID. Request: " +
                    request.id + ". Response: " + response.id;
        }

    }

    public static class ErrorResponseException extends DnsException {
        public ErrorResponseException(DnsQueryResult result) {
            super("Received " + result.response.responseCode + " error response\n" + result);
        }
    }

    public static class NoQueryPossibleException extends DnsException {


        public NoQueryPossibleException() {
            super("No DNS server could be queried");
        }

    }
}
